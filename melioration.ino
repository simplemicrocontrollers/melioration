// melioration 0.1
#include <Arduino.h>
#include <Thread.h>
#include <DS1307RTC.h>
const byte channel1 = 2;
const byte channel2 = 3;
const byte channel3 = 4;
const byte channel4 = 5;
const byte channel5 = 6;
const byte led = 13;
const byte button = 14; // A0

long previousButtonMillis = 0;
int buttonState = 0;
long buttonPressed = 0;
byte ledState = 0;
byte activeChannel = 0;
long channel5workingtime = 0;
Thread pressButtonThread = Thread();

void setup() {
  Serial.begin(9600);
  while (!Serial);
  setSyncProvider(RTC.get);
  if(timeStatus()!= timeSet) 
    Serial.println("tt");
  pinMode(channel1, OUTPUT);
  digitalWrite(channel1, HIGH);
  pinMode(channel2, OUTPUT);
  digitalWrite(channel2, HIGH);
  pinMode(channel3, OUTPUT);
  digitalWrite(channel3, HIGH);
  pinMode(channel4, OUTPUT);
  digitalWrite(channel4, HIGH);
  pinMode(channel5, OUTPUT);
  digitalWrite(channel5, HIGH);
  pinMode(button, INPUT);
  pinMode(led, OUTPUT);
  pressButtonThread.onRun(pressButton);
  pressButtonThread.setInterval(200); // Interval for checking button pressing - 200ms
  digitalWrite(led, HIGH);
  signalblinking();
  serStr("strt");
}

void loop() {
  // Thread init:
  if (pressButtonThread.shouldRun())
    pressButtonThread.run();
  // Check RTC.
  if (timeStatus() == timeSet) {
    checkTime();
  } else {
    serStr("settime");
    delay (10000);
  }
}

// Check button pressing thread
void pressButton() {
  unsigned long currentMillis = millis();
  if (channel5workingtime > 0) {
    if ((currentMillis - channel5workingtime) > 900000) { // 15 min channel5
      channelOff();
      delay(5000);
      channel5workingtime = 0;
    }
  }
  // Button on/off channel 5
  if (digitalRead(button) == LOW) buttonState = 1;
    else buttonState = 0;
  if (buttonState == 1) {
    buttonPressed=buttonPressed+200;
    if (buttonPressed > 5678) {
      serStr("long press");
      // nothing
      buttonPressed = 0;
    }
    previousButtonMillis = currentMillis;
    if (activeChannel == 5) {
      channelOff();
      channel5workingtime = 0;
    } else {
      channel5on();
      channel5workingtime = currentMillis;
    }
  }
  // Led indication
  ledState = ledState + 1;
  if (ledState < 6) {
    digitalWrite(led, LOW);
  } else if (ledState > 5) {
    digitalWrite(led, HIGH);
  }
  if (ledState > 9) ledState = 0;
 
}
 
// Send string to serial monitor with millis() counter and date/time
void serStr(String serString) {
  long millisTime = millis();
  String delimiter = "|";
  String currentTime = "";
  currentTime = currentTime + year() + "-" + month() + "-" + day() + "-" + hour() + "-" + minute() + "-" + second();
  String stringToPrint = millisTime + delimiter + currentTime + delimiter + serString;
  Serial.println(stringToPrint);
}

void signalblinking() {
  int signalblink;
  for (signalblink = 1; signalblink <= 5; signalblink++) {
    digitalWrite(led, HIGH);
    delay(50);
    digitalWrite(led, LOW);
    delay(50);
  }
}

void channel1on() {
  if (activeChannel != 1) { 
    signalblinking();
    digitalWrite(channel1, LOW);
    delay(1000);
    digitalWrite(channel2, HIGH);
    digitalWrite(channel3, HIGH);
    digitalWrite(channel4, HIGH);
    digitalWrite(channel5, HIGH);
    activeChannel = 1;
  }
}

void channel2on() {
  if (activeChannel != 2) { 
    signalblinking();
    digitalWrite(channel2, LOW);
    delay(1000);
    digitalWrite(channel1, HIGH);
    digitalWrite(channel3, HIGH);
    digitalWrite(channel4, HIGH);
    digitalWrite(channel5, HIGH);
    activeChannel = 2;
  }
}

void channel3on() {
  if (activeChannel != 3) { 
    signalblinking();
    digitalWrite(channel3, LOW);
    delay(1000);
    digitalWrite(channel1, HIGH);
    digitalWrite(channel2, HIGH);
    digitalWrite(channel4, HIGH);
    digitalWrite(channel5, HIGH);
    activeChannel = 3;
  }
}

void channel4on() {
  if (activeChannel != 4) {
    signalblinking();
    digitalWrite(channel4, LOW);
    delay(1000);
    digitalWrite(channel1, HIGH);
    digitalWrite(channel2, HIGH);
    digitalWrite(channel3, HIGH);
    digitalWrite(channel5, HIGH);
    activeChannel = 4;
  }
}

void channel5on() {
  if (activeChannel != 5) {
    unsigned long currentMillis = millis();
    if (channel5workingtime > 0) {
      if ((currentMillis - channel5workingtime) > 900000) {             // 15 min channel5
        channelOff();
        delay(5000);
        channel5workingtime = 0;
      }
    }
    signalblinking();
    digitalWrite(channel5, LOW);
    delay(1000);
    digitalWrite(channel1, HIGH);
    digitalWrite(channel2, HIGH);
    digitalWrite(channel3, HIGH);
    digitalWrite(channel4, HIGH);
    channel5workingtime = currentMillis;
    activeChannel = 5;
  }
  digitalWrite(led, HIGH);
}

void channelOff() {
  if (activeChannel != 0) {
    signalblinking();
    digitalWrite(channel1, HIGH);
    digitalWrite(channel2, HIGH);
    digitalWrite(channel3, HIGH);
    digitalWrite(channel4, HIGH);
    digitalWrite(channel5, HIGH);
    activeChannel = 0;
  }
}

// Main Timer:
void checkTime() {
  if (channel5workingtime == 0) {
    // 8-10 melioration
    if (hour() == 8 && minute() < 15) channel1on();
    if (hour() == 8 && minute() > 14 && minute() < 30) channel2on();
    if (hour() == 8 && minute() > 29 && minute() < 45) channel3on();
    if (hour() == 8 && minute() == 45) channelOff();
    if (hour() == 9 && minute() < 15) channel1on();
    if (hour() == 9 && minute() > 14 && minute() < 30) channel2on();
    if (hour() == 9 && minute() > 29 && minute() < 45) channel3on();
    if (hour() == 9 && minute() == 45) channelOff();
    // 13-15 melioration
    if (hour() == 13 && minute() < 15) channel1on();
    if (hour() == 13 && minute() > 14 && minute() < 30) channel2on();
    if (hour() == 13 && minute() > 29 && minute() < 45) channel3on();
    if (hour() == 13 && minute() == 45) channelOff();
    if (hour() == 14 && minute() < 15) channel1on();
    if (hour() == 14 && minute() > 14 && minute() < 30) channel2on();
    if (hour() == 14 && minute() > 29 && minute() < 45) channel3on();
    if (hour() == 14 && minute() == 45) channelOff();
    // 18-20 melioration
    if (hour() == 18 && minute() < 15) channel1on();
    if (hour() == 18 && minute() > 14 && minute() < 30) channel2on();
    if (hour() == 18 && minute() > 29 && minute() < 45) channel3on();
    if (hour() == 18 && minute() == 45) channelOff();
    if (hour() == 19 && minute() < 15) channel1on();
    if (hour() == 19 && minute() > 14 && minute() < 30) channel2on();
    if (hour() == 19 && minute() > 29 && minute() < 45) channel3on();
    if (hour() == 19 && minute() == 45) channelOff();
    if ((day() % 2) == 0) {
      // 1 hour per 2 days greenhouse melioration
      if (hour() == 8 && minute() > 45) channel4on();
      if (hour() == 9 && minute() > 45) channel4on();
      if (hour() == 10 && minute() == 2) channelOff();
      if (hour() == 18 && minute() > 45) channel4on();
      if (hour() == 19 && minute() > 45) channel4on();
      if (hour() == 20 && minute() == 2) channelOff();
    }
  }
}
